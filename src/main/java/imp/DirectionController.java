package imp;

import dto.AspiratorDTO;
import dto.GridDTO;
import dto.CommandEnum;

import java.util.List;

public class DirectionController {

    private static final int DISTANCE = 1;

    public static AspiratorDTO getAspiratorPosition(GridDTO grid, AspiratorDTO aspiratorDTO, List<CommandEnum> commandList) {

        for ( CommandEnum commandEnum : commandList) {
            switch ( commandEnum ) {
                case A -> {
                    switch ( aspiratorDTO.getOrientation() ) {
                        case 0 -> {
                            aspiratorDTO.setY(aspiratorDTO.getY() + DISTANCE);
                            break;
                        }
                        case 1 -> {
                            aspiratorDTO.setX(aspiratorDTO.getX() + DISTANCE);
                            break;
                        }
                        case 2 -> {
                            aspiratorDTO.setY(aspiratorDTO.getY() - DISTANCE);
                            break;
                        }
                        case 3 -> {
                            aspiratorDTO.setX(aspiratorDTO.getX() - DISTANCE);
                            break;
                        }
                    }
                    checkBorderLimit(grid, aspiratorDTO);
                    break;
                }
                case G -> {
                    aspiratorDTO.setOrientationAndCommand(3 + aspiratorDTO.getOrientation());
                    break;
                }
                case D -> {
                    aspiratorDTO.setOrientationAndCommand(1 + aspiratorDTO.getOrientation());
                    break;
                }
                default -> {
                    break;
                }
            }
        }
        return aspiratorDTO;
    }

    public static void checkBorderLimit(GridDTO grid, AspiratorDTO aspiratorDTO) {
        if (aspiratorDTO.getY() > grid.getHeight() || aspiratorDTO.getX() > grid.getWidth()) {
            System.out.println("la destination de l'aspirateur est Hors cadre !!!!");
            System.exit(0);
        }
    }
}
